w3m-el-snapshot (1.4.632+0.20210805.2351.f77da8b-1) unstable; urgency=medium

  * New upstream version 1.4.632+0.20210805.2351.f77da8b
  * Update debian/copyright

 -- Tatsuya Kinoshita <tats@debian.org>  Sun, 15 Aug 2021 08:58:28 +0900

w3m-el-snapshot (1.4.632+0.20210507.0231.2f95627-1) experimental; urgency=medium

  * New upstream version 1.4.632+0.20210507.0231.2f95627
  * Update debian/copyright

 -- Tatsuya Kinoshita <tats@debian.org>  Sat, 08 May 2021 14:16:15 +0900

w3m-el-snapshot (1.4.632+0.20210201.2305.54c3ccd-1) unstable; urgency=medium

  * New upstream version 1.4.632+0.20210201.2305.54c3ccd

 -- Tatsuya Kinoshita <tats@debian.org>  Thu, 04 Feb 2021 23:34:59 +0900

w3m-el-snapshot (1.4.632+0.20210106.2144.f29aada-1) unstable; urgency=medium

  * New upstream version 1.4.632+0.20210106.2144.f29aada
  * Update debian/copyright

 -- Tatsuya Kinoshita <tats@debian.org>  Wed, 20 Jan 2021 23:01:27 +0900

w3m-el-snapshot (1.4.632+0.20201224.0004.085de54-1) unstable; urgency=medium

  * New upstream version 1.4.632+0.20201224.0004.085de54
    - Move cursor to target dir when browsing local dir (closes: #973666)
  * Update Standards-Version to 4.5.1

 -- Tatsuya Kinoshita <tats@debian.org>  Sat, 26 Dec 2020 15:17:49 +0900

w3m-el-snapshot (1.4.632+0.20201021.0552.a4edf91-1) unstable; urgency=medium

  * New upstream version 1.4.632+0.20201021.0552.a4edf91
    - Work for a url like "...#36" (closes: #969744)
    - Allow url like "file:foo.txt" (closes: #969386)
  * Drop emacs25
  * Add Documentation to debian/upstream/metadata
  * Update debian/copyright

 -- Tatsuya Kinoshita <tats@debian.org>  Sun, 25 Oct 2020 23:04:06 +0900

w3m-el-snapshot (1.4.632+0.20200702.0409.dca01f9-1) unstable; urgency=medium

  * New upstream version 1.4.632+0.20200702.0409.dca01f9
  * Update debhelper-compat to 13

 -- Tatsuya Kinoshita <tats@debian.org>  Tue, 07 Jul 2020 22:59:56 +0900

w3m-el-snapshot (1.4.632+0.20200406.2209.e522a44-1) unstable; urgency=medium

  * New upstream version 1.4.632+0.20200406.2209.e522a44
    - Improve the links visivility (closes: #953631)
    - Abolish own horizontal scrolling stuff (closes: #953768)
  * Add debian/upstream/metadata
  * Update Description from emacsen to GNU Emacs
  * Update debian/watch to use mode=git

 -- Tatsuya Kinoshita <tats@debian.org>  Sun, 26 Apr 2020 18:46:32 +0900

w3m-el-snapshot (1.4.632+0.20200302.2210.03ea43b-1) unstable; urgency=medium

  * New upstream version 1.4.632+0.20200302.2210.03ea43b
  * Update debian/copyright
  * Update Standards-Version to 4.5.0

 -- Tatsuya Kinoshita <tats@debian.org>  Fri, 06 Mar 2020 22:40:39 +0900

w3m-el-snapshot (1.4.632+0.20191218.2243.cef0c7e-1) unstable; urgency=medium

  * New upstream version 1.4.632+0.20191218.2243.cef0c7e
    - Work for downloading empty file (closes: #944574)
    - Quote lang spec defined in w3m-accept-languages (closes: #944519)
  * Add Rules-Requires-Root: no
  * Update Standards-Version to 4.4.1

 -- Tatsuya Kinoshita <tats@debian.org>  Sun, 22 Dec 2019 23:03:59 +0900

w3m-el-snapshot (1.4.632+0.20190920.1116.c9cdb7e-1) unstable; urgency=medium

  * New upstream version 1.4.632+0.20190920.1116.c9cdb7e
  * Update debian/copyright
  * Set w3m-use-symbol to nil in emacsen-startup (closes: #934434)
  * Use Upstream-Contact to mention upstream sites
  * Add the get-orig-source target
  * Set emacs-w3m-git-revision with debian/changelog

 -- Tatsuya Kinoshita <tats@debian.org>  Sun, 22 Sep 2019 19:31:24 +0900

w3m-el-snapshot (1.4.632+0.20190731-1) unstable; urgency=medium

  * New upstream version 1.4.632+0.20190731
    - Fix incorrect use of w3m-url-strip-query (closes: #933371)
    - Fix broken w3m-scroll-up-or-next-url
  * Add GitHub information (closes: #933542)

 -- Tatsuya Kinoshita <tats@debian.org>  Wed, 31 Jul 2019 23:11:46 +0900

w3m-el-snapshot (1.4.632+0.20190719-1) unstable; urgency=medium

  * New upstream version 1.4.632+0.20190719
    - Fix installation failure with emacs-nox

 -- Tatsuya Kinoshita <tats@debian.org>  Fri, 19 Jul 2019 23:27:18 +0900

w3m-el-snapshot (1.4.632+0.20190712-2) unstable; urgency=medium

  * Add semi to Suggests and handle dependency for mime-w3m.el
  * Update debhelper-compat to 12

 -- Tatsuya Kinoshita <tats@debian.org>  Tue, 16 Jul 2019 21:47:13 +0900

w3m-el-snapshot (1.4.632+0.20190712-1) experimental; urgency=low

  * New upstream version 1.4.632+0.20190712
  * Drop emacs23, emacs24, xemacs21 and mule-ucs
  * Update debian/copyright
  * Update Standards-Version to 4.4.0

 -- Tatsuya Kinoshita <tats@debian.org>  Fri, 12 Jul 2019 23:03:15 +0900

w3m-el-snapshot (1.4.632+0.20190607-1) unstable; urgency=medium

  * New upstream version 1.4.632+0.20190607

 -- Tatsuya Kinoshita <tats@debian.org>  Fri, 05 Jul 2019 23:30:49 +0900

w3m-el-snapshot (1.4.632+0.20190528-1) experimental; urgency=low

  * Typo fix for Standards-Version
  * Drop emacs22
  * New upstream version 1.4.632+0.20190528
  * Update debian/copyright

 -- Tatsuya Kinoshita <tats@debian.org>  Wed, 29 May 2019 21:41:15 +0900

w3m-el-snapshot (1.4.632+0.20190419-1) experimental; urgency=low

  * New upstream version 1.4.632+0.20190419
  * Don't define emacs-w3m-git-revision even if GitHub version
  * Set w3m-use-refresh to nil in emacsen-startup
  * Update debian/copyright
  * Update Standards-Version to 4.3.0

 -- Tatsuya Kinoshita <tats@debian.org>  Sat, 20 Apr 2019 17:31:36 +0900

w3m-el-snapshot (1.4.632+0.20181112-2) unstable; urgency=medium

  * Don't remove *.el files when handling reverse dependency
  * No longer recompile emacspeak
  * Use debhelper-compat 11

 -- Tatsuya Kinoshita <tats@debian.org>  Mon, 17 Dec 2018 21:37:00 +0900

w3m-el-snapshot (1.4.632+0.20181112-1) unstable; urgency=medium

  * New upstream version 1.4.632+0.20181112
  * Update debian/copyright
  * Update Standards-Version to 4.2.1

 -- Tatsuya Kinoshita <tats@debian.org>  Tue, 27 Nov 2018 21:10:52 +0900

w3m-el-snapshot (1.4.631+0.20180620-2) unstable; urgency=medium

  * Fix handling for unversioned emacs flavor
  * Fix handling for xemacs21-mule/nomule
  * Check stamp file so that source file isn't newer
  * Update Standards-Version to 4.2.0

 -- Tatsuya Kinoshita <tats@debian.org>  Tue, 21 Aug 2018 21:19:05 +0900

w3m-el-snapshot (1.4.631+0.20180620-1) unstable; urgency=medium

  * New upstream version 1.4.631+0.20180620
  * Handle symlinks for emacsen-startup

 -- Tatsuya Kinoshita <tats@debian.org>  Thu, 28 Jun 2018 22:59:51 +0900

w3m-el-snapshot (1.4.625+0.20180518-2) unstable; urgency=medium

  * Accept unversioned emacs flavor for emacsen-common 3.0.0
  * Drop workaround for emacsen-common 1.x (closes: #736898)

 -- Tatsuya Kinoshita <tats@debian.org>  Sun, 03 Jun 2018 14:51:09 +0900

w3m-el-snapshot (1.4.625+0.20180518-1) unstable; urgency=medium

  * New upstream version 1.4.625+0.20180518
  * Update debhelper compat version to 11
  * Migrate from anonscm.debian.org to salsa.debian.org
  * Update debian/copyright
  * Update Standards-Version to 4.1.4

 -- Tatsuya Kinoshita <tats@debian.org>  Fri, 01 Jun 2018 20:23:27 +0900

w3m-el-snapshot (1.4.609+0.20171225-1) unstable; urgency=medium

  * New upstream version 1.4.609+0.20171225
  * Update debhelper compat version to 10
  * Update Priority to optional
  * Update Vcs-Git to https
  * Update debian/copyright
  * Update Standards-Version to 4.1.3
  * Prevent declare-function errors in emacsen-install

 -- Tatsuya Kinoshita <tats@debian.org>  Sat, 06 Jan 2018 11:04:36 +0900

w3m-el-snapshot (1.4.569+0.20170110-1) unstable; urgency=medium

  * New upstream version 1.4.569+0.20170110

 -- Tatsuya Kinoshita <tats@debian.org>  Wed, 18 Jan 2017 22:12:47 +0900

w3m-el-snapshot (1.4.568+0.20170106-1) unstable; urgency=medium

  * New upstream version 1.4.568+0.20170106
  * Prefer emacs-nox over emacs
  * Update debhelper compat version to 9
  * Update debian/copyright

 -- Tatsuya Kinoshita <tats@debian.org>  Sat, 07 Jan 2017 19:05:29 +0900

w3m-el-snapshot (1.4.567+0.20161006-1) unstable; urgency=medium

  * Imported Upstream version 1.4.567+0.20161006
  * Update debian/copyright
  * Accept emacs25

 -- Tatsuya Kinoshita <tats@debian.org>  Sun, 09 Oct 2016 12:40:11 +0900

w3m-el-snapshot (1.4.560+0.20160422-1) unstable; urgency=medium

  * Imported Upstream version 1.4.560+0.20160422
  * Update debian/copyright
  * Update Standards-Version to 3.9.8
  * Add libtext-markdown-perl to Suggests

 -- Tatsuya Kinoshita <tats@debian.org>  Wed, 25 May 2016 22:31:02 +0900

w3m-el-snapshot (1.4.560+0.20160304-1) unstable; urgency=medium

  * Imported Upstream version 1.4.560+0.20160304
  * Update debian/copyright
  * Add discount | markdown to Suggests
  * Update Standards-Version to 3.9.7

 -- Tatsuya Kinoshita <tats@debian.org>  Fri, 04 Mar 2016 19:46:16 +0900

w3m-el-snapshot (1.4.556+0.20151109-1) unstable; urgency=medium

  * Imported Upstream version 1.4.556+0.20151109
  * Update Vcs-Browser to https

 -- Tatsuya Kinoshita <tats@debian.org>  Fri, 20 Nov 2015 22:27:48 +0900

w3m-el-snapshot (1.4.555+0.20150929-1) unstable; urgency=medium

  * Imported Upstream version 1.4.555+0.20150929
  * Update debian/copyright

 -- Tatsuya Kinoshita <tats@debian.org>  Thu, 01 Oct 2015 21:06:01 +0900

w3m-el-snapshot (1.4.554+0.20150609-1) unstable; urgency=medium

  * Imported Upstream version 1.4.554+0.20150609

 -- Tatsuya Kinoshita <tats@debian.org>  Mon, 20 Jul 2015 23:32:28 +0900

w3m-el-snapshot (1.4.548+0.20150510-1) unstable; urgency=medium

  * Imported Upstream version 1.4.548+0.20150510
    - Avoid Google search looping

 -- Tatsuya Kinoshita <tats@debian.org>  Tue, 12 May 2015 00:06:58 +0900

w3m-el-snapshot (1.4.544+0.20150406-1) unstable; urgency=medium

  * Imported Upstream version 1.4.544+0.20150406
  * Update debian/copyright
  * Update Vcs-Browser

 -- Tatsuya Kinoshita <tats@debian.org>  Sat, 25 Apr 2015 19:07:23 +0900

w3m-el-snapshot (1.4.538+0.20141022-1) unstable; urgency=medium

  * Imported Upstream version 1.4.538+0.20141022
  * Update debian/copyright

 -- Tatsuya Kinoshita <tats@debian.org>  Wed, 22 Oct 2014 19:18:00 +0900

w3m-el-snapshot (1.4.538+0.20141007-1) unstable; urgency=medium

  * Imported Upstream version 1.4.538+0.20141007
  * Update debian/copyright
  * Update Standards-Version to 3.9.6

 -- Tatsuya Kinoshita <tats@debian.org>  Fri, 17 Oct 2014 23:07:59 +0900

w3m-el-snapshot (1.4.537+0.20141001-1) unstable; urgency=medium

  * Imported Upstream version 1.4.537+0.20141001
  * Update debian/copyright
  * Use configure option --with-emacs=emacs

 -- Tatsuya Kinoshita <tats@debian.org>  Mon, 06 Oct 2014 19:12:23 +0900

w3m-el-snapshot (1.4.537+0.20140804-1) unstable; urgency=medium

  * Imported Upstream version 1.4.537+0.20140804

 -- Tatsuya Kinoshita <tats@debian.org>  Tue, 19 Aug 2014 23:14:41 +0900

w3m-el-snapshot (1.4.535+0.20140730-1) unstable; urgency=medium

  * Imported Upstream version 1.4.535+0.20140730

 -- Tatsuya Kinoshita <tats@debian.org>  Thu, 31 Jul 2014 19:37:34 +0900

w3m-el-snapshot (1.4.533+0.20140611-1) unstable; urgency=medium

  * Imported Upstream version 1.4.533+0.20140611

 -- Tatsuya Kinoshita <tats@debian.org>  Mon, 16 Jun 2014 21:04:40 +0900

w3m-el-snapshot (1.4.531+0.20140421-2) unstable; urgency=medium

  * Depend on emacsen-common 2.0.8 (closes: #736898)
  * Do not install obsolete attic/*.el files

 -- Tatsuya Kinoshita <tats@debian.org>  Thu, 22 May 2014 18:48:06 +0900

w3m-el-snapshot (1.4.531+0.20140421-1) unstable; urgency=medium

  * Imported Upstream version 1.4.531+0.20140421

 -- Tatsuya Kinoshita <tats@debian.org>  Tue, 22 Apr 2014 21:44:03 +0900

w3m-el-snapshot (1.4.531+0.20140331-1) unstable; urgency=medium

  * Imported Upstream version 1.4.531+0.20140331
  * Update debian/copyright

 -- Tatsuya Kinoshita <tats@debian.org>  Mon, 31 Mar 2014 22:06:23 +0900

w3m-el-snapshot (1.4.527+0.20140210-1) unstable; urgency=medium

  * Imported Upstream version 1.4.527+0.20140210
  * Install a compat file with emacsen-compat
  * Add emacsen-common (<< 2.0.0) to Conflicts
  * Handle an installed file to follow emacsen-common 2.0.7

 -- Tatsuya Kinoshita <tats@debian.org>  Mon, 10 Feb 2014 19:43:20 +0900

w3m-el-snapshot (1.4.527+0.20140108-1) unstable; urgency=medium

  * Imported Upstream version 1.4.527+0.20140108
  * Update debian/copyright

 -- Tatsuya Kinoshita <tats@debian.org>  Sun, 26 Jan 2014 19:54:37 +0900

w3m-el-snapshot (1.4.527+0.20131217-1) unstable; urgency=low

  * Imported Upstream version 1.4.527+0.20131217
  * Workaround for emacsen-common <2 and debhelper <9.20131104

 -- Tatsuya Kinoshita <tats@debian.org>  Thu, 26 Dec 2013 23:38:30 +0900

w3m-el-snapshot (1.4.527+0.20131206-1) unstable; urgency=low

  * Imported Upstream version 1.4.527+0.20131206
  * Add workaround to compatible with emacsen-common <2.0.0 (closes: #480239)

 -- Tatsuya Kinoshita <tats@debian.org>  Sun, 08 Dec 2013 14:40:40 +0900

w3m-el-snapshot (1.4.527+0.20131204-1) unstable; urgency=low

  * Imported Upstream version 1.4.527+0.20131204
    - Prevent infinite loop in mew-w3m.

 -- Tatsuya Kinoshita <tats@debian.org>  Wed, 04 Dec 2013 23:37:29 +0900

w3m-el-snapshot (1.4.527+0.20131202-1) unstable; urgency=low

  * Imported Upstream version 1.4.527+0.20131202
  * Update Standards-Version to 3.9.5

 -- Tatsuya Kinoshita <tats@debian.org>  Mon, 02 Dec 2013 20:10:44 +0900

w3m-el-snapshot (1.4.517+0.20130801-1) unstable; urgency=low

  * Imported Upstream version 1.4.517+0.20130801

 -- Tatsuya Kinoshita <tats@debian.org>  Thu, 08 Aug 2013 21:20:14 +0900

w3m-el-snapshot (1.4.517+0.20130704-2) unstable; urgency=low

  * Drop emacs21 to prevent an installation failure (closes: #717293)

 -- Tatsuya Kinoshita <tats@debian.org>  Sun, 28 Jul 2013 18:55:02 +0900

w3m-el-snapshot (1.4.517+0.20130704-1) unstable; urgency=low

  * Imported Upstream version 1.4.517+0.20130704
  * Update debian/copyright

 -- Tatsuya Kinoshita <tats@debian.org>  Sat, 06 Jul 2013 09:57:11 +0900

w3m-el-snapshot (1.4.515+0.20130621-1) unstable; urgency=low

  * Imported Upstream version 1.4.515+0.20130621
  * Update debian/copyright

 -- Tatsuya Kinoshita <tats@debian.org>  Tue, 25 Jun 2013 21:50:06 +0900

w3m-el-snapshot (1.4.513+0.20130419-1) unstable; urgency=low

  * Imported Upstream version 1.4.513+0.20130419
  * debian/control: Add Vcs-Git and Vcs-Browser

 -- Tatsuya Kinoshita <tats@debian.org>  Mon, 06 May 2013 14:57:30 +0900

w3m-el-snapshot (1.4.513+0.20130405-1) experimental; urgency=low

  * New upstream release (CVS trunk on 2013-04-05)
  * debian/copyright: Updated
  * debian/control: Update Standards-Version to 3.9.4

 -- Tatsuya Kinoshita <tats@debian.org>  Sat, 06 Apr 2013 23:45:30 +0900

w3m-el-snapshot (1.4.483+0.20120614-2) unstable; urgency=low

  * debian/control: Add emacs24

 -- Tatsuya Kinoshita <tats@debian.org>  Thu, 05 Jul 2012 21:43:22 +0900

w3m-el-snapshot (1.4.483+0.20120614-1) unstable; urgency=low

  * New upstream release (CVS trunk on 2012-06-14)
    - Fix labels macro for Emacs 24.1.50 (closes: #677431)

 -- Tatsuya Kinoshita <tats@debian.org>  Thu, 14 Jun 2012 19:21:38 +0900

w3m-el-snapshot (1.4.482+0.20120609-1) unstable; urgency=low

  * New upstream release (CVS trunk on 2012-06-09)

 -- Tatsuya Kinoshita <tats@debian.org>  Sun, 10 Jun 2012 10:59:58 +0900

w3m-el-snapshot (1.4.478+0.20120501-1) unstable; urgency=low

  * New upstream release. (CVS trunk on 2012-05-01)
  * debian/control: Don't mention a stable version in Description.
  * debian/copyright: Updated.

 -- Tatsuya Kinoshita <tats@debian.org>  Thu, 03 May 2012 22:42:49 +0900

w3m-el-snapshot (1.4.471+0.20120228-1) unstable; urgency=low

  * New upstream release. (CVS trunk on 2012-02-28)
    - Work for name anchors. (closes: #657424)
  * debian/copyright: Update copyright-format version to 1.0.
  * debian/control: Update Standards-Version to 3.9.3.

 -- Tatsuya Kinoshita <tats@debian.org>  Sun, 11 Mar 2012 19:28:43 +0900

w3m-el-snapshot (1.4.463+0.20120113-1) unstable; urgency=low

  * New upstream release. (CVS trunk on 2012-01-13)
  * debian/copyright: Updated.

 -- Tatsuya Kinoshita <tats@debian.org>  Mon, 23 Jan 2012 22:36:53 +0900

w3m-el-snapshot (1.4.442+0.20110729-1) unstable; urgency=low

  * New upstream release. (CVS trunk on 2011-07-29)
    - Work around the Emacs 24 error "`keymap' is reserved for embedded
      parent maps". (closes: #637726)
  * debian/rules: Add the targets build-indep and build-arch.

 -- Tatsuya Kinoshita <tats@debian.org>  Sun, 14 Aug 2011 14:32:35 +0900

w3m-el-snapshot (1.4.438+0.20110511-1) unstable; urgency=low

  * New upstream release. (CVS trunk on 2011-05-11)
  * debian/emacsen-install.in: Add messages for the rare situations.

 -- Tatsuya Kinoshita <tats@debian.org>  Mon, 20 Jun 2011 20:34:00 +0900

w3m-el-snapshot (1.4.437+0.20110422-1) unstable; urgency=low

  * New upstream release. (CVS trunk on 2011-04-22)
  * debian/control: Update Standards-Version to 3.9.2.
  * debian/copyright: Switch to the DEP-5 format.

 -- Tatsuya Kinoshita <tats@debian.org>  Sun, 24 Apr 2011 20:51:42 +0900

w3m-el-snapshot (1.4.433+0.20110201-1) unstable; urgency=low

  * New upstream release. (CVS trunk on 2011-02-01)
  * debian/control: Update Standards-Version to 3.9.1.
  * debian/copyright: Updated.

 -- Tatsuya Kinoshita <tats@debian.org>  Sun, 06 Feb 2011 15:22:34 +0900

w3m-el-snapshot (1.4.400+0.20100725-1) unstable; urgency=low

  * New upstream release. (CVS trunk on 2010-07-25)
  * debian/control:
    - Add GNU Emacs flavors to Build-Depends-Indep.
    - Update Standards-Version to 3.9.0.
  * debian/copyright: Updated.

 -- Tatsuya Kinoshita <tats@debian.org>  Sun, 25 Jul 2010 21:09:11 +0900

w3m-el-snapshot (1.4.394+0.20100524-1) unstable; urgency=low

  * New upstream release. (CVS trunk on 2010-05-24)
    - Fix that w3m-cookie-reject-domains doesn't work. (closes: #582677)

 -- Tatsuya Kinoshita <tats@debian.org>  Mon, 24 May 2010 22:50:08 +0900

w3m-el-snapshot (1.4.393+0.20100421-1) unstable; urgency=low

  * New upstream release. (CVS trunk on 2010-04-21)

 -- Tatsuya Kinoshita <tats@debian.org>  Sat, 08 May 2010 16:12:29 +0900

w3m-el-snapshot (1.4.392+0.20100329-1) unstable; urgency=low

  * New upstream release. (CVS trunk on 2010-03-29)
  * debian/rules: Install shimbun/ChangeLog* and ChangeLog.*.
  * debian/copyright: Updated.

 -- Tatsuya Kinoshita <tats@debian.org>  Tue, 30 Mar 2010 01:32:35 +0900

w3m-el-snapshot (1.4.387+0.20100304-1) unstable; urgency=low

  * New upstream release. (CVS trunk on 2010-03-04)
  * debian/copyright: Updated.
  * Switch to dpkg-source 3.0 (quilt) format.

 -- Tatsuya Kinoshita <tats@debian.org>  Thu, 04 Mar 2010 21:05:42 +0900

w3m-el-snapshot (1.4.384+0.20100219-1) unstable; urgency=low

  * New upstream release. (CVS trunk on 2010-02-19)
    - Bug fix for sb-rss.el and sb-atom.el.

 -- Tatsuya Kinoshita <tats@debian.org>  Sat, 20 Feb 2010 11:53:05 +0900

w3m-el-snapshot (1.4.384+0.20100218-1) unstable; urgency=low

  * New upstream release. (CVS trunk on 2010-02-18)
  * debian/copyright: Updated.

 -- Tatsuya Kinoshita <tats@debian.org>  Thu, 18 Feb 2010 22:56:01 +0900

w3m-el-snapshot (1.4.374+0.20100112-1) unstable; urgency=low

  * New upstream release. (CVS trunk on 2010-01-12)
    - Don't fail even if an anchor string is broken. (closes: #537327)
  * debian/control:
    - Add emacs-snapshot to Depends. (closes: #506106)
    - Add `dpkg (>= 1.15.4) | install-info' to Depends.
    - Add ${misc:Depends} to Depends.
    - Set Section to lisp.
    - Remove emacsen flavors from Build-Depends-Indep, remain `emacs'.
    - Update Standards-Version to 3.8.4.
  * debian/emacsen-install.in: Don't fail w3m-el 1.4.4 with emacs23 and
    emacs-snapshot.
  * debian/copyright: Updated.

 -- Tatsuya Kinoshita <tats@debian.org>  Sat, 13 Feb 2010 00:53:08 +0900

w3m-el-snapshot (1.4.364+0.20090802-1) unstable; urgency=low

  * New upstream release. (CVS trunk on 2009-08-02)
  * debian/control:
    - Add emacs23 to Depends.
    - Update Standards-Version to 3.8.2.
  * debian/copyright: Updated.

 -- Tatsuya Kinoshita <tats@debian.org>  Sun, 02 Aug 2009 23:40:20 +0900

w3m-el-snapshot (1.4.344+0.20090405-1) unstable; urgency=low

  * New upstream release. (CVS trunk on 2009-04-05)
  * debian/emacsen-install: Don't fail with xemacs21-nomule.
  * debian/control: Update Standards-Version to 3.8.1.
  * debian/copyright: Updated.

 -- Tatsuya Kinoshita <tats@debian.org>  Thu, 09 Apr 2009 00:46:00 +0900

w3m-el-snapshot (1.4.335+0.20090216-1) unstable; urgency=low

  * New upstream release. (CVS trunk on 2009-02-16)
    - Fix anchor charset. (closes: #479631)
  * debian/emacsen-*: Rewritten.
  * debian/rules, debian/dirs.in: Install attic/*.el in the site-lisp
    directory.
  * debian/docs: Install NEWS* and BUGS*.
  * debian/w3mhack.el.patch: Removed.
  * debian/compat, debian/control: Update debhelper version to 7.
  * debian/control:
    - Remove bitmap-mule from Suggests.
    - Update Standards-Version to 3.8.0.
  * debian/rules: Use dh_prep instead of `dh_clean -k'.
  * debian/copyright: Updated.

 -- Tatsuya Kinoshita <tats@debian.org>  Mon, 16 Feb 2009 23:05:55 +0900

w3m-el-snapshot (1.4.263+0.20080321-1) unstable; urgency=low

  * New upstream release. (CVS trunk on 2008-03-21)
  * debian/control: Build-Depends: debhelper (>= 6).

 -- Tatsuya Kinoshita <tats@debian.org>  Fri, 21 Mar 2008 22:36:26 +0900

w3m-el-snapshot (1.4.263+0.20080317-1) unstable; urgency=low

  * New upstream release. (CVS trunk on 2008-03-17)
    - w3m.el (w3m-mouse-safe-view-this-url): Remove redundant confirmation
      of whether to follow link. (closes: #468257)
  * debian/compat: 5 -> 6.
  * debian/copyright: Updated.

 -- Tatsuya Kinoshita <tats@debian.org>  Wed, 19 Mar 2008 00:32:16 +0900

w3m-el-snapshot (1.4.258+0.20080220-2) unstable; urgency=low

  * debian/emacsen-startup.in: Set w3m-icon-directory to
    "/usr/share/pixmaps/w3m-el/small" for GNU Emacs.

 -- Tatsuya Kinoshita <tats@debian.org>  Sat, 23 Feb 2008 07:08:15 +0900

w3m-el-snapshot (1.4.258+0.20080220-1) unstable; urgency=low

  * New upstream release. (CVS trunk on 2008-02-20)
  * debian/rules: Install *.gif and *.png.

 -- Tatsuya Kinoshita <tats@debian.org>  Wed, 20 Feb 2008 23:29:12 +0900

w3m-el-snapshot (1.4.257+0.20080207-1) unstable; urgency=low

  * New upstream release. (CVS trunk on 2008-02-07)
    - Set show-trailing-whitespace to nil. (closes: #457115)
  * debian/copyright: Updated.

 -- Tatsuya Kinoshita <tats@debian.org>  Sun, 17 Feb 2008 18:04:43 +0900

w3m-el-snapshot (1.4.250+0.20071206-1) unstable; urgency=low

  * New upstream release. (CVS trunk on 2007-12-06)
  * debian/control:
    - (Conflicts): Add epoch to the version of flim, and remove mew.
    - (Depends): Remove the version of apel.
    - (Description): Typo fix.
    - (Homepage): Move from Description.
    - (Standards-Version): 3.7.2 -> 3.7.3.

 -- Tatsuya Kinoshita <tats@debian.org>  Sun, 09 Dec 2007 23:19:35 +0900

w3m-el-snapshot (1.4.244+0.20071127-1) unstable; urgency=low

  * New upstream release. (CVS trunk on 2007-11-27)
  * debian/control: Add w3m-el to Provides.
  * debian/copyright: Updated.
  * Use /usr/share/pixmaps/w3m-el instead of
    /usr/share/pixmaps/w3m-el-snapshot.

 -- Tatsuya Kinoshita <tats@debian.org>  Wed, 28 Nov 2007 22:29:38 +0900

w3m-el-snapshot (1.4.239+0.20071122-2) unstable; urgency=low

  * debian/control: Set Priority to extra.

 -- Tatsuya Kinoshita <tats@debian.org>  Sun, 25 Nov 2007 16:26:39 +0900

w3m-el-snapshot (1.4.239+0.20071122-1) unstable; urgency=low

  * Initial release. (CVS trunk on 2007-11-22, closes: #442459)

 -- Tatsuya Kinoshita <tats@debian.org>  Sun, 25 Nov 2007 15:04:58 +0900
